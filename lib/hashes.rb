# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  result = Hash.new
  str.split(" ").each do |word|
    result[word] = word.length
  end
  result
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  count = Hash.new(0)
  word.split("").each do |letter|
    count[letter] += 1
  end
  count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  arr.each do |el|
    hash[el] += 1
  end
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  count = Hash.new(0)
  numbers.each do |num|
    if num.odd?
      count[:odd] += 1
    else
      count[:even] += 1
    end
  end
  count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  count = Hash.new(0)
  string.split("").each do |char|
    count[char] += 1
  end
  max_count = count.values.max
  new = count.select { |k, v| v == max_count }
  new.keys.sort_by {|letter| letter}[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  later_half_keys = students.select { |k, v| v > 6 }.keys
  return later_half_keys.combination(2).to_a
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  count = Hash.new(0)
  specimens.each do |specimen|
    count[specimen] += 1
  end
  size_count = count.values.sort_by { |v| v }
  (count.keys.length ** 2) * (size_count.first / size_count.last)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_hash = Hash.new(0)
  normal_sign.split(" ").each do |word|
    word.split("").each do |letter|
      normal_hash[letter.downcase] += 1
    end
  end

  van_hash = Hash.new(0)
  vandalized_sign.split(" ").each do |word|
    word.split("").each do |letter|
      van_hash[letter.downcase] += 1
    end
  end

  van_hash.each do |k, v|
    return false if normal_hash[k] < v
  end
  true

end

def character_count(str)
end
